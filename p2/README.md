# Project 2

## Requirements
    - Complete the application using MVC framework
	- Modify CustomerServlet.java and CustomerDB.java
	- Connect web app to database
	- Add CRUD functionality to data storage
    
[Link to webapp](http://localhost:9999/lis4368/customerform.jsp?assign_num=p2)

![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/1.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/2.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/3.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/4.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/5.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/6.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/9dff71160b3fba49367eeb0a5d267228a2b9e163/p2/screenshots/7.PNG)
