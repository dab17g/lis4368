# Assignment 5

## Requirements
    - Using code from a4 use server side validation
	- Modify and add classes to insert data in mysql database
	- Upload screenshots of three stages: pre-submission, post-submission, and database screenshot
    
[Link to webapp](http://localhost:9999/lis4368/customerform.jsp?assign_num=a5)

![](https://bitbucket.org/dab17g/lis4368/raw/556a423e3d537f3e288afc788887e25f49c4c564/a5/screenshots/1.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/556a423e3d537f3e288afc788887e25f49c4c564/a5/screenshots/2.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/556a423e3d537f3e288afc788887e25f49c4c564/a5/screenshots/3.PNG)