# Assignment 1

### Assignment Requirements
- Provide read only access to bitbucket repository
- Install JDK
- Install ampps
- Install Apache Tomcat
- Clone directory to cd C:/tomcat/webapps

### Git command descriptions:
1. git innit: used to initialize a local directory to become a git repository. The first thing you should do when making a repository.
2. git status: Displays the status of the directory. Also shows the files in the staging area.
3. git add: adds the files that have been changed or added to the staging area.
4. git commmit: Takes a screenshot of the files/changes in the staging area.
5. git push: Pushes the commit to the remote repository.
6. git pull: Downloads the files/changes that are present in the remote repository but not the local repository to the local repository.
7. git clone: Used to clone a remote repository to a local directory.

[Link to web app](http://localhost:9999/lis4368/)

[Link to bitbucketlocationstations](https://bitbucket.org/dab17g/bitbucketstationlocations/src)

![](https://bitbucket.org/dab17g/lis4368/raw/5c4d4c950ae3f59e2bc6536810cd870252ad913b/a1/img/javaHello.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/0d7cebff00b6f06ad5cfe679480626d91578c335/a1/img/tomcat.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/d4ffc98047e6e1a44451bb7638b9adedc3e85d31/a1/img/webapp.PNG)