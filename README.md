# LIS 4368 - Advanced Web Application Development

## Dathan Busby

### Assignment Links:

1. [A1 README.md](https://bitbucket.org/dab17g/lis4368/src/master/a1)
	- Provide read only access to bitbucket repository
    - Install JDK
    - Install ampps
    - Install Apache Tomcat
    - Clone directory to cd C:/tomcat/webapps
	
2. [A2 README.md](https://bitbucket.org/dab17g/lis4368/src/master/a2)
	- Create hello directory with index.html
	- Create Hello Again page
	- Create querybook page and results page
	- Complete A2 question
	
3. [A3 README.md](https://bitbucket.org/dab17g/lis4368/src/master/a3)
	- Create erd
	- Update inserts
	- upload screenshots to bitbucket

4. A4 README.md

5. [A5 README.md](https://bitbucket.org/dab17g/lis4368/src/master/a5)
	- Using code from a4 use server side validation
	- Modify and add classes to insert data in mysql database
	- Upload screenshots of three stages: pre-submission, post-submission, and database screenshot

6. [P1 README.md](https://bitbucket.org/dab17g/lis4368/src/master/p1)
	- Create home page with slideshow content
	- Add inserts to table form page
	- Modify javascript on index file
	- Use regex to do client side form validation
	- Upload screenshots

7. [P2 README.md](https://bitbucket.org/dab17g/lis4368/src/master/p2)
	- Complete the application using MVC framework
	- Modify CustomerServlet.java and CustomerDB.java
	- Connect web app to database
	- Add CRUD functionality to data storage

[bitbucketlocationstations link](https://bitbucket.org/dab17g/bitbucketstationlocations/src/master/)
	
>>>>>>> 155aacb2b347db584d9dbd4e007fd14b21af2e38
