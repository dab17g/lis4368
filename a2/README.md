# Assignment 2

### Assignment Requirements
- Login to MySQL using ampps
- Create web pages on tomcat
- Develop and deploy servlets to tomcat server

[Link to LIS4368 web app](http://localhost:9999/lis4368/)

[Link to hello web app](http://localhost:9999/hello/index.html)

![](https://bitbucket.org/dab17g/lis4368/raw/185990244b4cd727dd7cefffae5c22b7d9ff1616/a2/img/home.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/185990244b4cd727dd7cefffae5c22b7d9ff1616/a2/img/sayHello.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/e5cb599956d0ba4ee5ca45e75cd97f965ae9009d/a2/img/helloagain.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/185990244b4cd727dd7cefffae5c22b7d9ff1616/a2/img/querybook.PNG)
![](https://bitbucket.org/dab17g/lis4368/raw/185990244b4cd727dd7cefffae5c22b7d9ff1616/a2/img/result.PNG)