# Assignment 3

### Assignment Requirements
- Create ERD
- Forward engineer ERD
- Display table data
- Display screenshots

[Link to LIS4368 web app](http://localhost:9999/lis4368/)

[Link to hello web app](http://localhost:9999/hello/index.html)

![](https://bitbucket.org/dab17g/lis4368/raw/2f4e360331d33663bd602a4ca92ae12f9955200c/a3/img/erd.PNG)

[a3.SQL](https://bitbucket.org/dab17g/lis4368/src/master/a3/docs/a3_busby.sql)
[a3.MWB](https://bitbucket.org/dab17g/lis4368/src/master/a3/docs/erd.mwb)
